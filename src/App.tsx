import './App.css';
import Chat from './components/Chat/Chat';

const App = (): JSX.Element => {
  return (
    <div className="app">
      <Chat url="https://edikdolynskyi.github.io/react_sources/messages.json" />
    </div>
  );
};

export default App;
