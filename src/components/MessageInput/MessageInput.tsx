import { useEffect, useState } from 'react';
import './MessageInput.css';

interface IMessageInputProps {
  onSendMessage: (messageText: string) => void;
  editMessage: { id: string; text: string };
  onSaveEdit: (text: string) => void;
  onCancelEdit: () => void;
}

const MessageInput = ({
  onSendMessage,
  editMessage,
  onSaveEdit,
  onCancelEdit,
}: IMessageInputProps): JSX.Element => {
  const [messageText, setMessageText] = useState(editMessage.text || '');

  useEffect(() => {
    if (editMessage.id) {
      setMessageText(editMessage.text);
    } else {
      setMessageText('');
    }
  }, [editMessage]);
  const handleInput = (e: React.FormEvent<HTMLInputElement>) => {
    setMessageText(e.currentTarget.value);
  };

  return (
    <form
      className="message-input"
      onSubmit={(e) => {
        e.preventDefault();
        onSendMessage(messageText);
        setMessageText('');
      }}
    >
      <input
        className="message-input-text"
        type="text"
        onChange={handleInput}
        value={messageText || ''}
      />
      {editMessage.id !== '' ? (
        <>
          <button
            className="message-input-button"
            type="button"
            onClick={() => onSaveEdit(messageText)}
          >
            Save
          </button>
          <button
            className="message-input-button"
            type="button"
            onClick={onCancelEdit}
          >
            Cancel
          </button>
        </>
      ) : (
        <button className="message-input-button" type="submit">
          Send
        </button>
      )}
    </form>
  );
};

export default MessageInput;
