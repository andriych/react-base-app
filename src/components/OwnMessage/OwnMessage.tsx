import { useEffect, useRef } from 'react';
import { getHoursMins } from '../../helpers/date-format';
import { IMessage } from '../../types/IMessage';
import './OwnMessage.css';

interface IOwnMessageProps {
  message: IMessage;
  onMessageEdit: (id: string, text: string) => void;
  onMessageDelete: (id: string) => void;
}

const OwnMessage = ({
  message,
  onMessageEdit,
  onMessageDelete,
}: IOwnMessageProps): JSX.Element => {
  const { id, avatar, user, text, createdAt, editedAt } = message;

  return (
    <div className="own-message">
      <button
        type="button"
        className="message-edit"
        onClick={() => onMessageEdit(id, text)}
      >
        ✏️
      </button>
      <button
        type="button"
        className="message-delete"
        onClick={() => onMessageDelete(id)}
      >
        ❌
      </button>
      <div className="message-info">
        <div className="message-details">
          <p className="message-user-name">{user}</p>
          {editedAt ? (
            <>
              edited at
              <p className="message-time">{`${getHoursMins(editedAt)}`}</p>
            </>
          ) : (
            <p className="message-time">{`${getHoursMins(createdAt)}`}</p>
          )}
        </div>
        <p className="message-text">{text}</p>
      </div>
      <img src={avatar} alt="user" className="message-avatar" />
    </div>
  );
};

export default OwnMessage;
