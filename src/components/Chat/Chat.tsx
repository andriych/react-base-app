import { useState, useEffect } from 'react';
import Header from '../Header/Header';
import './Chat.css';
import MessageList from '../MessageList/MessageList';
import MessageInput from '../MessageInput/MessageInput';
import { IMessage } from '../../types/IMessage';
import { guid } from '../../helpers/generate-id';

interface IChatProps {
  url: string;
}

const currentUserId = '1111243930-83c9-11e9-8e0c-8f1a686f4ce4';

const Chat = ({ url }: IChatProps): JSX.Element => {
  const [sortedMessages, setSortedMessages] = useState<IMessage[]>([]);
  const [editMessage, setEditMessage] = useState({ id: '', text: '' });

  useEffect(() => {
    fetch(url)
      .then((res) => res.json())
      .then((res) => {
        setSortedMessages(sortListByDate(res));
      });
  }, [url]);

  const getUsersCount = (): number => {
    const uniqueUsers = new Set();
    sortedMessages.forEach((msg) => uniqueUsers.add(msg.userId));

    return uniqueUsers.size;
  };

  const sortListByDate = (messages: IMessage[]): IMessage[] => {
    const sortedMessages: IMessage[] = messages.sort((msgOne, msgTwo) => {
      return +new Date(msgOne.createdAt) - +new Date(msgTwo.createdAt);
    });

    return sortedMessages;
  };

  const formatDate = (fullDate: string): string => {
    if (!fullDate) return '';

    const date = fullDate.split('T')[0];
    const time = fullDate.split('T')[1];
    const dateDay = date.slice(8, 10);
    const dateMonth = date.slice(5, 7);
    const dateYear = date.slice(0, 4);
    const formatedTime = time.slice(0, 5);

    return dateDay + '.' + dateMonth + '.' + dateYear + ' ' + formatedTime;
  };

  const handleSendMessage = (messageText: string): void => {
    const timezonedTime = new Date();
    timezonedTime.setHours(timezonedTime.getHours() + 2);

    const message: IMessage = {
      id: guid(),
      userId: currentUserId,
      avatar:
        'https://resizing.flixster.com/kr0IphfLGZqni5JOWDS2P1-zod4=/280x250/v1.cjs0OTQ2NztqOzE4NDk1OzEyMDA7MjgwOzI1MA',
      user: 'Me',
      text: messageText,
      createdAt: timezonedTime.toISOString(),
      editedAt: '',
    };

    setSortedMessages(sortListByDate([...sortedMessages, message]));
  };

  const handleEditMessage = (id: string, text: string) => {
    setEditMessage({ id, text });
  };

  const handleDeleteMessage = (id: string) => {
    sortedMessages.forEach((msg, i) => {
      if (msg.id === id) {
        sortedMessages.splice(i, 1);
      }
    });

    setSortedMessages([...sortedMessages]);
  };

  const handleSaveEdit = (text: string) => {
    const timezonedTime = new Date();
    timezonedTime.setHours(timezonedTime.getHours() + 2);

    const message = sortedMessages.find((msg) => msg.id === editMessage.id);
    if (message) {
      message.text = text;
      message.editedAt = timezonedTime.toISOString();
    }

    setEditMessage({ id: '', text: '' });
  };

  const handleCancelEdit = () => {
    setEditMessage({ id: '', text: '' });
  };

  return (
    <main className="chat">
      <div className="container">
        <Header
          title="My chat"
          usersCount={getUsersCount()}
          messagesCount={sortedMessages.length}
          lastMessageDate={formatDate(sortedMessages.slice(-1)[0]?.createdAt)}
        />
        <MessageList
          messages={sortedMessages}
          onMessageEdit={handleEditMessage}
          onMessageDelete={handleDeleteMessage}
        />
        <MessageInput
          onSendMessage={handleSendMessage}
          editMessage={editMessage}
          onSaveEdit={handleSaveEdit}
          onCancelEdit={handleCancelEdit}
        />
      </div>
    </main>
  );
};

export default Chat;
