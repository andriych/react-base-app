import { IMessage } from '../../types/IMessage';
import Message from '../Message/Message';
import OwnMessage from '../OwnMessage/OwnMessage';
import Preloader from '../Preloader/Preloader';
import './MessageList.css';

enum Days {
  Sunday,
  Monday,
  Tuesday,
  Wednesday,
  Thursday,
  Friday,
  Saturday,
}

enum Months {
  January,
  February,
  March,
  April,
  May,
  June,
  July,
  August,
  September,
  October,
  November,
  December,
}

interface IMessageListProps {
  messages: IMessage[];
  onMessageEdit: (id: string, text: string) => void;
  onMessageDelete: (id: string) => void;
}

const currentUserId = '1111243930-83c9-11e9-8e0c-8f1a686f4ce4';

const MessageList = ({
  messages,
  onMessageEdit,
  onMessageDelete,
}: IMessageListProps): JSX.Element => {
  const isDay = (date: Date, dayOffset = 0) => {
    const today = new Date();
    return (
      date.getDate() === today.getDate() + dayOffset &&
      date.getMonth() === today.getMonth() &&
      date.getFullYear() === today.getFullYear()
    );
  };

  const getOutputDividerDate = (date: string): string => {
    let day = '';
    const currentDate = new Date(date);

    if (isDay(currentDate)) {
      day = 'Today';
    } else if (isDay(currentDate, -1)) {
      day = 'Yesterday';
    } else {
      day = `${Days[currentDate.getDay()]}, ${currentDate.getDate()} ${
        Months[currentDate.getMonth()]
      }`;
    }

    return day;
  };

  const getOutputDivider = (date: string): JSX.Element => {
    const day = getOutputDividerDate(date);
    return (
      <div className="messages-divider-wrapper">
        <span className="messages-divider">{day}</span>
        <div className="messages-divider-line"></div>
      </div>
    );
  };

  const getOutputMessage = (message: IMessage): JSX.Element => {
    const msg =
      message.userId !== currentUserId ? (
        <Message key={message.id} message={message} />
      ) : (
        <OwnMessage
          key={message.id}
          message={message}
          onMessageEdit={onMessageEdit}
          onMessageDelete={onMessageDelete}
        />
      );
    return msg;
  };

  if (!messages.length) {
    return (
      <div className="message-list">
        <Preloader></Preloader>
      </div>
    );
  }

  let prevDividerDate = '';

  return (
    <div className="message-list">
      {messages.map((msg, i) => {
        if (prevDividerDate === getOutputDividerDate(msg.createdAt)) {
          return getOutputMessage(msg);
        } else {
          prevDividerDate = getOutputDividerDate(msg.createdAt);
          return (
            <div key={i}>
              {getOutputDivider(msg.createdAt)}
              {getOutputMessage(msg)}
            </div>
          );
        }
      })}
    </div>
  );
};

export default MessageList;
