export const getHoursMins = (date: string) => date.split('T')[1].slice(0, 5);
